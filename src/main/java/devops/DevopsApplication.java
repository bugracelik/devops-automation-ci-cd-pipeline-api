package devops;

import org.apache.tomcat.jni.Library;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.annotation.Native;

@SpringBootApplication
@RestController
public class DevopsApplication {

	public static void main(String[] args) {
		SpringApplication.run(DevopsApplication.class, args);
	}

	@GetMapping("deploy-run-script")
	public String deploy()
	{
		String path="/repositories/devops/devops.sh";
		File file=new File(path);

		if (file.exists()) {
			System.out.println("read="+file.canRead());
			System.out.println("write="+file.canWrite());
			System.out.println("Execute="+file.canExecute());
			file.setReadOnly();
			file.setExecutable(true);
			file.setWritable(true);
		}

		SSHRunner.run("/repositories/devops/devops.sh");
		return "deploy-success";
	}


}

class SSHRunner {

	public static void run(String sshPath) {
		Process p;
		try {
			String[] cmd = {"sh", sshPath};
			p = Runtime.getRuntime().exec(cmd);
			p.waitFor();
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					p.getInputStream()));
			String line;
			while ((line = reader.readLine()) != null) {
				System.out.println(line);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}


